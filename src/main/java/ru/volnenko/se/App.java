package ru.volnenko.se;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ru.volnenko.se.config.SpringConfig;
import ru.volnenko.se.controller.Bootstrap;

public class App {

    public static void main(String[] args) throws Exception {
        AbstractApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
        context.close();
    }

}
