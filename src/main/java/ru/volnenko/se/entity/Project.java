package ru.volnenko.se.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * @author Denis Volnenko
 */

@Entity
@Table(name = "PROJECTS")
@NamedQueries({
        @NamedQuery(name = Project.QUERY_GET_BY_NAME, query = "SELECT p FROM Project p WHERE p.name = :name"),
        @NamedQuery(name = Project.QUERY_GET_ALL, query = "SELECT p FROM Project p"),
        @NamedQuery(name = Project.QUERY_DELETE_ALL, query = "DELETE FROM Project")
}
)
public final class Project implements Serializable {

    public static final String QUERY_GET_BY_NAME = "Project.getByName";
    public static final String QUERY_GET_ALL = "Project.getAll";
    public static final String QUERY_DELETE_ALL = "Project.getDeleteAll";

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String name = "";

    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

}
