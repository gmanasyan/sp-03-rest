package ru.volnenko.se.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.volnenko.se.api.service.*;
import ru.volnenko.se.command.AbstractCommand;

/**
 * @author Denis Volnenko
 */
@Controller
public final class Bootstrap {

    @Autowired
    private ITerminalService terminalService;

    public void init() throws Exception {
        terminalService.register();
        start();
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = terminalService.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = terminalService.getCommand(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

}
