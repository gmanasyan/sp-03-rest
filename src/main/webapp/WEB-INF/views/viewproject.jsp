<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Просмотр проекта</title>
    <link rel="stylesheet" href="assets/css/style.css">
<body>

<div class="header">
    <a href="projects">Projects</a>
    <a href="tasks">Tasks</a>
</div>

<div class="box">

<h2>View Project</h2>

    <form method="post" action="edit-project">
        <dl>
            <dt>Project ID:</dt>
            <dd>${project.id}</dd>
        </dl>
        <dl>
            <dt>Name of project:</dt>
            <dd>${project.name}</dd>
        </dl>
        <dl>
            <dt>Start date:</dt>
            <dd><fmt:formatDate value="${project.dateBegin}" pattern="yyyy-MM-dd"/></dd>
        </dl>
        <dl>
            <dt>End date:</dt>
            <dd><fmt:formatDate value="${project.dateEnd}" pattern="yyyy-MM-dd"/></dd>
        </dl>
    </form>
    <a class="button" onclick="window.history.back()">Back</a>
</div>
</body>
</html>